%global gem_name actionpack
%bcond_with bootstrap

Name:		rubygem-%{gem_name}
Epoch:		1
Version:	7.0.7
Release: 	5
Summary: 	Web-flow and rendering framework putting the VC in MVC (part of Rails)
License: 	MIT
URL: 		http://rubyonrails.org
Source0: 	https://rubygems.org/gems/%{gem_name}-%{version}.gem
# ActionPack gem doesn't ship with the test suite.
# You may check it out like so
# git clone http://github.com/rails/rails.git
# cd rails/actionpack && git archive -v -o actionpack-7.0.7-tests.txz v7.0.7 test/
Source1: 	%{gem_name}-%{version}-tests.txz
# The tools are needed for the test suite, are however unpackaged in gem file.
# You may get them like so
# git clone http://github.com/rails/rails.git --no-checkout
# cd rails && git archive -v -o rails-7.0.7-tools.txz v7.0.7 tools/
Source2: 	rails-%{version}-tools.txz
# Fixes for Minitest 5.16+
# https://github.com/rails/rails/pull/45370
Patch0: rubygem-actionpack-7.0.2.3-Fix-tests-for-minitest-5.16.patch
# https://github.com/rails/rails/commit/4c83b331092a79d58e4adffe4be5f250fa5782cc
Patch1: CVE-2024-26143.patch
Patch2: CVE-2024-26143-test.patch
Patch3: fix-CVE-2024-28103.patch

Patch3000:	backport-CVE-2024-41128.patch
Patch3001:	backport-CVE-2024-47887.patch

# Let's keep Requires and BuildRequires sorted alphabeticaly
BuildRequires: 	ruby(release)
BuildRequires: 	rubygems-devel
BuildRequires: 	ruby >= 2.2.2
%if %{without bootstrap}
BuildRequires: 	rubygem(activemodel) = %{version}
BuildRequires: 	rubygem(activerecord) = %{version}
BuildRequires: 	rubygem(activesupport) = %{version}
BuildRequires: 	rubygem(actionview) = %{version}
BuildRequires: 	rubygem(railties) = %{version}
BuildRequires: 	rubygem(rack)
BuildRequires: 	rubygem(rack-cache)
BuildRequires: 	rubygem(rack-test)
BuildRequires: 	rubygem(puma)
BuildRequires: 	rubygem(capybara) >= 3.26
BuildRequires: 	rubygem(selenium-webdriver)
BuildRequires: 	rubygem(rexml)
%endif
BuildRequires:  rubygem(did_you_mean) rubygem(matrix)
BuildArch: 	noarch

%description
Eases web-request routing, handling, and response as a half-way front,
half-way page controller. Implemented with specific emphasis on enabling easy
unit/integration testing that doesn't require a browser.

%package 	doc
Summary: 	Documentation for %{name}
Requires: 	%{name} = %{epoch}:%{version}-%{release}
BuildArch:	noarch

%description 	doc
Documentation for %{name}.

%prep
%setup -q -n %{gem_name}-%{version}%{?prerelease} -b1 -b2
%patch 1 -p2
%patch 3 -p2
%patch 3000 -p2
%patch 3001 -p2

pushd %{_builddir}
%patch 0 -p2
%patch 2 -p2
popd

%build
gem build ../%{gem_name}-%{version}%{?prerelease}.gemspec
%gem_install

%install
mkdir -p %{buildroot}%{gem_dir}
cp -a .%{gem_dir}/* \
        %{buildroot}%{gem_dir}/


%if %{without bootstrap}
%check
pushd .%{gem_instdir}
ln -s %{_builddir}/tools ..
cp -a %{_builddir}/test .
for tname in 'rendering a relative path with dot' 'rendering a relative path'; do
  sed -i "/^\s* test \"$tname\" do/ a \      skip" \
    test/controller/new_base/render_file_test.rb
done

find test -type f -name '*_test.rb' -print0 | \
  sort -z | \
  xargs -0 -n1 -i sh -c "echo '* Test file: {}'; ruby -Ilib:test -- '{}' || exit 255"

popd
%endif

%files
%dir %{gem_instdir}
%license %{gem_instdir}/MIT-LICENSE
%{gem_libdir}
%exclude %{gem_cache}
%{gem_spec}

%files doc
%doc %{gem_docdir}
%doc %{gem_instdir}/CHANGELOG.md
%doc %{gem_instdir}/README.rdoc

%changelog
* Thu Oct 31 2024 xuguangmin <xuguangmin@kylinos.cn> - 1:7.0.7-5
- fix: %patchN is deprecated (2 usages found), use %patch N (or %patch -P N) 

* Thu Oct 17 2024 yaoxin <yao_xin001@hoperun.com> - 1:7.0.7-4
- Fix CVE-2024-41128 and CVE-2024-47887

* Wed Jun 5 2024 xuchenchen <xuchenchen@kylinos.cn> - 1:7.0.7-3
- fix CVE-2024-28103

* Wed Feb 28 2024 yaoxin <yao_xin001@hoperun.com> - 1:7.0.7-2
- Fix CVE-2024-26143 and remove unused file

* Fri Aug 18 2023 chenchen <chen_aka_jan@163.com> - 1:7.0.7-1
- Upgrade to version 7.0.7

* Mon Jul 24 2023 wangkai <13474090681@163.com> - 1:7.0.4-3
- Fix CVE-2023-28362

* Mon Feb 20 2023 jiangpeng <jiangpeng01@ncti-gba.cn> - 1:7.0.4-2
- Fix CVE-2023-22797

* Fri Jan 20 2023 wangkai <wangkai385@h-partners.com> - 1:7.0.4-1
- Upgrade to version 7.0.4

* Thu Jun 30 2022 houyingchao <houyingchao@h-partners.com> - 1:6.1.4.1-2
- Fix compilation failed

* Thu May 03 2022 wangkerong <wangkerong@h-partners.com>- 6.1.4.1-1
- Upgrade to 6.1.4.1

* Mon Jun 28 2021 wangyue<wangyue92@huawei.com> - 5.2.4.4-3
- Fix CVE-2021-22904

* Fri Jun 11 2021 wangyue<wangyue92@huawei.com> - 5.2.4.4-2
- Fix CVE-2021-22885

* Mon Feb  8 2021 sunguoshuai<sunguoshuai@huawei.com>- 5.2.4.4-1
- Upgrade to 5.2.4.4

* Sat Aug 8 2020 chengzihan <chengzihan2@huawei.com> - 5.2.3-1
- Package init
